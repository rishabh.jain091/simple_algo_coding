/*
Given an array of integers nums which is sorted in ascending order, and an integer target, write a function to search target in nums. If target exists, then return its index. Otherwise, return -1.
You must write an algorithm with O(log n) runtime complexity


*/

class Solution {
public:
    int search(vector<int>& nums, int target) {
        int len = nums.size();
        int low = 0;
        int high = len - 1;
        return binary_search(nums, target, low, high);
    }

    int binary_search(vector<int>& nums, int target, int low, int high){
        if(low > high){
            return -1;
        }
        else{
            int mid = (low + high)/2;
            if(target == nums[mid]){
                return mid;
            }
            else if (target > nums[mid]){ // target is on the right
                return binary_search(nums, target, mid+1, high);
            }
            else{
                return binary_search(nums, target, low, mid-1);
            }

        }
    }
};